angular.module(AppConfig.name).constant('events.names', {
    USER: {
      ADD: 'user.add',
      UPDATE: 'user.update',
      REMOVE: 'user.remove',
      RETRIEVE: {
        ONE: 'user.retrieve.one',
        LIST: 'user.retrieve.list'
      }
    }
});
