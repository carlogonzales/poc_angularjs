var AppConfig = (function (window) {
  var angular = window.angular;
  var appName = 'pocAngularApp';
  var appDependencies = [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router'];

  /**
   * Register a new application module
   *
   * @param moduleName
   * @param dependencies
   */
  var registerModule = function(moduleName, dependencies){
    angular.module(moduleName, dependencies || []);
    angular.module(appName).requires.push(moduleName);
  };

  /**
   * Add new application module dependency
   *
   * TODO: Make it possible to add an array of dependency and remove duplicates
   *
   * @param moduleName
   * @param dependency
   */
  var addModuleDependency = function(moduleName, dependency){
    if(angular.module(appName).requires.indexOf(moduleName) < 0) {
      return;
    }

    angular.module(moduleName).requires.push(dependency);
  };

  var registerConstants = function(identifier, constants){
    angular.module(appName).config(identifier, constants);
  };

  return {
    name: appName,
    dependencies: appDependencies,
    registerModule: registerModule,
    addModuleDependency: addModuleDependency,
    registerConstants: registerConstants
  };
})(window);
