'use strict';

// Define application name and dependencies
angular.module(AppConfig.name, AppConfig.dependencies);

// Enable HTML5 location mode
angular.module(AppConfig.name).config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);

// Bootstrap application
angular.element(document).ready(function(){
  // initiallize app
  angular.bootstrap(document, [AppConfig.name]);
});
