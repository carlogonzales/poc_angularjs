'use strict';

/**
 * @ngdoc function
 * @name pocAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pocAngularApp
 */
angular.module('pocAngularApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
