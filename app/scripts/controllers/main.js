'use strict';

/**
 * @ngdoc function
 * @name pocAngularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pocAngularApp
 */
angular.module('pocAngularApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
