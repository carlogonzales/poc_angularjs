angular.module('modules.users').controller('UsersController', [
  '$scope', '$stateParams', 'UsersChannel',
  function($scope, $stateParams, UsersChannel){
    var onUserListRetrievalHandler = function(users){
      $scope.users = users;
    };
    var onUserRetrievalHandler = function(user) {
      $scope.user = user;
    };
    var onNewUserHandler = function(user){
      alert(user);
    };

    $scope.fetchUsers = function(){
      UsersChannel.retrieveUsers();
    };
    $scope.fetchOneUser = function(){
      UsersChannel.retrieveUserById($stateParams.userId);
    };
    $scope.addUser = function() {
      UsersChannel.addNewUser($scope.user);
    };

    UsersChannel.onUserListRetrieval($scope, onUserListRetrievalHandler);
    UsersChannel.onUserRetrieval($scope, onUserRetrievalHandler);
    UsersChannel.onNewUser($scope, onNewUserHandler);
  }]);
