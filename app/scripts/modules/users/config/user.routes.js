angular.module('modules.users')
  .config(['$stateProvider', function($stateProvider){

    $stateProvider
      .state('users', {
        abstract: true,
        templateUrl: 'scripts/modules/users/views/users.html.tpl'
      })
      .state('users.list', {
        url: '/users/list',
        templateUrl: 'scripts/modules/users/views/users.list.html.tpl'
      })
      .state('users.add', {
        url: '/users/add',
        templateUrl: 'scripts/modules/users/views/users.add.html.tpl'
      });
  }]);
