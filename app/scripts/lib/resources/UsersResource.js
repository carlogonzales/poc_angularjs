angular.module('resources')
  .factory('UsersResource', [
    'LoDash',
    function(_){
      var currentIndex = 0;
      var usersDatabase = {};

      /**
       * Retrieve a user by ID
       * @param id
       * @returns User
       */
      var retrieveById = function(id){
        if(!_.isNumber(id)) return;
        return usersDatabase[id];
      };

      /**
       * Retrieve all users
       * @returns {Array}
       */
      var retrieveAll = function(){
        return _.map(usersDatabase);
      };

      /**
       * Add new user
       * @param user
       */
      var addNewUser = function(user){
        if(!_.isPlainObject(user)) return;
        user.id = currentIndex;
        usersDatabase[currentIndex++] = user;
      };

      /**
       * Update a user
       * @param user
       */
      var updateUser = function(user){
        if(!_.has(user, 'id')) return;
        if(!_.has(usersDatabase, '' + user.id)) return;
        usersDatabase[user.id] = user;
      };

      /**
       * Remove a user
       * @param user
       */
      var removeUser = function(user){
        if(!_.has(user, 'id')) return;
        if(!_.has(usersDatabase, '' + user.id)) return;
        delete usersDatabase[user.id];
      };

      return {
        retrieveById: retrieveById,
        retrieveAll: retrieveAll,
        add: addNewUser,
        update: updateUser,
        remove: removeUser
      };
  }]);
