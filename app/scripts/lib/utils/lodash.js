angular.module('utils')
  .factory('LoDash', function($window){
    var _ = $window._;
    delete $window._;
    return (_);
  });
