'use strict';

angular.module('channels')
  .factory('UsersChannel', [
    '$rootScope', 'UsersResource', 'events.names',
    function($rootScope, UsersResource, EVENT){
      /**
       * Add new user
       *
       * TODO: Add checking if successful
       *
       * @param user
       */
      var addNewUser = function(user){
        UsersResource.add(user);
        $rootScope.$broadcast(EVENT.USER.ADD, user);
      };

      /**
       * Update a user
       *
       * TODO: Add checking if successful
       *
       * @param user
       */
      var updateUser = function(user){
        UsersResource.update(user);
        $rootScope.$broadcast(EVENT.USER.UPDATE, user);
      };

      /**
       * Remove a user
       *
       * TODO: Add checking if successful
       *
       * @param user
       */
      var removeUser = function(user){
        UsersResource.remove(user);
        $rootScope.$broadcast(EVENT.USER.REMOVE, user);
      };

      /**
       * Retrieve the list of users
       */
      var retrieveUsers = function(){
        $rootScope.$broadcast(EVENT.USER.RETRIEVE.LIST, UsersResource.retrieveAll());
      };

      var retrieveUserById = function(userId){
        $rootScope.$broadcast(EVENT.USER.RETRIEVE.ONE, UsersResource.retrieveById(userId));
      };

      var onNewUser = function($scope, eventHandler){
        $scope.$on(EVENT.USER.ADD, function(event, user){
          eventHandler(user);
        });
      };

      var onUserUpdate = function($scope, eventHandler){
        $scope.$on(EVENT.USER.UPDATE, function(event, user){
          eventHandler(user);
        });
      };

      var onUserRemove = function($scope, eventHandler){
        $scope.$on(EVENT.USER.REMOVE, function(event, user){
          eventHandler(user);
        });
      };

      var onUserListRetrieval = function($scope, eventHandler){
        $scope.$on(EVENT.USER.RETRIEVE.LIST, function(event, users){
          eventHandler(users);
        });
      };

      var onUserRetrieval = function($scope, eventHandler) {
        $scope.$on(EVENT.USER.RETRIEVE.ONE, function(event, user){
          eventHandler(user);
        });
      };

      return {
        addNewUser: addNewUser,
        updateUser: updateUser,
        removeUser: removeUser,
        retrieveUsers: retrieveUsers,
        retrieveUserById: retrieveUserById,
        onNewUser: onNewUser,
        onUserUpdate: onUserUpdate,
        onUserRemove: onUserRemove,
        onUserListRetrieval: onUserListRetrieval,
        onUserRetrieval: onUserRetrieval
      };
    }
]);
